//
//  Grid.h
//  Example1
//
//  Created by Николай on 08.05.17.
//  Copyright © 2017 Experiments. All rights reserved.
//

#import <UIKit/UIKit.h>

struct GridCoordinate{
    int row;
    int column;
};
typedef struct GridCoordinate GridCoordinate;

@interface Grid : NSObject

@property (nonatomic, readonly) int count;
@property (nonatomic, readonly) int columns;
@property (nonatomic, readonly) int rows;
@property (nonatomic, readonly) double rowHeight;
@property (nonatomic, readonly) double columnWidth;

- (Grid*) initUniformWithColumns:(int)columns screenSize:(CGSize)size;
- (Grid*) initUniformWithColumns:(int)columns screenSize:(CGSize)size padding:(double)padding;

- (Grid*) initWithRows:(int)rows
              coulumns:(int)columns
            screenSize:(CGSize)size
       paddingVertical:(double)paddingVertical
     paddingHorizontal:(double)paddingHorizontal;

- (Grid*) initWithRows:(int)rows
              coulumns:(int)columns
            screenSize:(CGSize)size
       padding:(double)padding;

- (Grid*) initWithRows:(int)rows coulumns:(int)columns screenSize:(CGSize)size;


- (CGRect) rectAtIndex:(int)index;
- (CGRect) rectAtRow:(int)row column:(int)column;
- (int) indexAtRow:(int)row column:(int)column;
- (int) indexAtPoint:(CGPoint)point;
- (GridCoordinate) coordinateAtPoint:(CGPoint)point;

@end
