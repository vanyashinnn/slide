//
//  UIImage+Crop.m
//  Slide
//
//  Created by Николай on 11.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import "UIImage+Crop.h"

@implementation UIImage (Crop)
- (UIImage *)cropWithRect:(CGRect)rect fromSize:(CGSize)size {
    
    double coeffw = self.size.width/size.width;
    double coeffh = self.size.height/size.height;
    rect.size.width *= coeffw;
    rect.origin.x *= coeffw;
    rect.size.height *= coeffh;
    rect.origin.y *= coeffh;
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    return result;
}

- (UIImage *)cropForRatio:(double) ratio{
    CGSize sz = self.size;
    if(ratio >1){
        sz.height = sz.width/ratio;
    }else{
        sz.width = sz.height*ratio;
    }

    CGRect rect = CGRectMake(
                             (self.size.width-sz.width)/2,
                             (self.size.height-sz.height)/2,
                             sz.width,
                             sz.height
                             );
    
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return result;
}
@end
