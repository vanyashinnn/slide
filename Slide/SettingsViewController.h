//
//  SettingsViewController.h
//  Slide
//
//  Created by Николай on 21.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectImageView.h"

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet SelectImageView *selectImageView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
