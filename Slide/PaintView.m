//
//  PaintView.m
//  Example1
//
//  Created by Николай on 30.04.17.
//  Copyright © 2017 Experiments. All rights reserved.
//

#import "PaintView.h"
#import "Grid.h"
#import "UIImage+Crop.h"
#import "Settings.h"

static const int TOUCHES_MAX = 10;
static const int TILES_ROWS_COLUMNS_MAX = 1000;

static const int AUTO_MOVE_STEPS_COUNT = 10;
static const int MIX_COUNT = 5;

enum Direction{
    DIRECTION_HORIZONTAL = 0,
    DIRECTION_VERTICAL,
    DIRECTION_UNKNOWN
};
typedef enum Direction Direction;

enum TouchEvent{
    TOUCH_UP = 0,
    TOUCH_DOWN,
    TOUCH_MOVE
};
typedef enum TouchEvent TouchEvent;

struct Touch{
    GridCoordinate startCoordinate;
    GridCoordinate currentCoordinate;
    // Номер плитки, на которую нажали
    int startIndex;
    // Номер плитки, над которой сейчас курсор
    int currentIndex;
    // Позиция птитки, на которую нажали
    CGPoint origin;
    // Координаты, на которые нажали
    CGPoint start;
    // Текущие координаты
    CGPoint current;
};
typedef struct Touch Touch;


static int randomIn(int min, int max){
    return rand()%(max-min+1)+min;
}

@interface PaintView(){
    UIImageView *_background;
    Touch touchArray[TOUCHES_MAX];
    int moveBitMap;
    int moveCount[TILES_ROWS_COLUMNS_MAX];
    int tilesInfo[TILES_ROWS_COLUMNS_MAX][TILES_ROWS_COLUMNS_MAX];
    int counter;
    int mixCounter;
    BOOL isMixed;
    NSTimer *_timer;
}
@property (strong, nonatomic) Grid * grid;
@property (strong, nonatomic) NSMutableArray * images;
@property (strong, nonatomic) NSMutableArray * extraColumnImages;
@property (strong, nonatomic) NSMutableArray * extraRowImages;
@property (nonatomic) Direction direction;
@property (nonatomic) int touchesCounter;

- (void) drawRow:(NSNumber*)row delta:(NSNumber*)delta;
- (void) drawColumn:(NSNumber*)column delta:(NSNumber*)delta;
- (BOOL) isComplete;
@end


@implementation PaintView

#pragma mark -
#pragma mark Перемешивание

-(int) randomBitsFrom:(int)from to:(int)to count:(int)count{
    NSLog(@"bits:");
    int bits = 0;
    for(int i=0; i<count; ++i){
        int bit;
        while(YES){
            bit = randomIn(from, to);
            if((bits & (1 << bit)) == 0){
                bits |= (1 << bit);
                NSLog(@"%d", bit);
                break;
            }
        }
    }
    return bits;
}
-(void)mix{
    isMixed = YES;
    mixCounter = 0;
    [self mixLoop];
}
-(void)mixLoop{
    if(mixCounter >= MIX_COUNT){
        isMixed = NO;
        return;
    }
    if(mixCounter%2 == 0){
        [self autoMoveRows:2];
    }else{
        [self autoMoveColumns:2];
    }
    ++mixCounter;
}

-(void) autoMoveColumns:(int)count{
    [self autoMoveInitWithRows:self.grid.columns columns:self.grid.rows count:count];
    [self startTimerWithSelector:@selector(moveColumnsTimeout:)];
}
-(void) moveColumnsTimeout:(NSTimer*)timer{
    [self move:self.grid.columns :self.grid.rowHeight :@selector(drawColumn:delta:) :@selector(moveTilesColumn:count:)];
}
-(void) autoMoveRows:(int)count{
    [self autoMoveInitWithRows:self.grid.rows columns:self.grid.columns count:count];
    [self startTimerWithSelector:@selector(moveRowsTimeout:)];
}
-(void) moveRowsTimeout:(NSTimer*)timer{
    [self move:self.grid.rows :self.grid.columnWidth :@selector(drawRow:delta:) :@selector(moveTilesRow:count:)];
}
-(void) autoMoveInitWithRows:(int)rows columns:(int)columns count:(int)count{
    moveBitMap = [self randomBitsFrom:0 to:(rows-1) count:count];
    for(int row=0; row<rows; ++row){
        if((moveBitMap & (1 << row)) == 0){
            continue;
        }
        while(YES){
            moveCount[row] = randomIn(-columns, columns);
            if(moveCount[row]%columns != 0){
                break;
            }
        }
    }
    counter = 0;
}
-(void) move:(int)n :(double)h :(SEL)draw :(SEL)complete{
    ++counter;
    for(int i=0; i<n; ++i){
        if((moveBitMap & (1 << i)) == 0){
            continue;
        }
        double ratio = (double)(moveCount[i] * h)/AUTO_MOVE_STEPS_COUNT;
        double d = (counter % AUTO_MOVE_STEPS_COUNT) * ratio;
        [self performSelector:draw withObject:[NSNumber numberWithInt:i] withObject:[NSNumber numberWithDouble:d]];
    }
    if(counter >= AUTO_MOVE_STEPS_COUNT){
        for(int i=0; i<n; ++i){
            if((moveBitMap & (1 << i)) == 0){
                continue;
            }
            [self performSelector:complete withObject:[NSNumber numberWithInt:i] withObject:[NSNumber numberWithInt:moveCount[i]]];
        }
        [self stopTimer];
        [self mixLoop];
    }
}

- (void)stopTimer{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}
- (void)startTimerWithSelector:(SEL)timerSelector{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:timerSelector
                                                userInfo:nil
                                                 repeats:YES];
    }
}


#pragma mark - Применение
- (void) moveTilesRow:(NSNumber*)rowObj count:(NSNumber*)countObj{
    int row = [rowObj intValue];
    int count = [countObj intValue];
    count = count%self.grid.columns;
    if(count < 0){
        count += self.grid.columns;
    }
    printf("moveTilesRow:%d count:%d\nbefore:", row, count);
    for(int column=0; column<self.grid.columns; ++column){
        printf("\t%d", tilesInfo[row][column]);
        int idx = (column+self.grid.columns-count)%self.grid.columns;
        tilesInfo[self.grid.rows][column] = tilesInfo[row][idx];
    }
    printf("\nafter:");
    for(int column=0; column<self.grid.columns; ++column){
        tilesInfo[row][column] = tilesInfo[self.grid.rows][column];
        printf("\t%d", tilesInfo[row][column]);
        UIImageView * image = [self.images objectAtIndex: tilesInfo[row][column]] ;
        image.frame = [self.grid rectAtRow:row column:column];
    }
    CGRect rect = [self.grid rectAtRow:row column:(self.grid.columns-1)];
    rect.origin.x += self.grid.columnWidth;
    UIImageView * extraImage = [self.extraRowImages objectAtIndex: row];
    extraImage.frame = rect;
    printf("\n");
}

- (void) moveTilesColumn:(NSNumber*)columnObj count:(NSNumber*)countObj{
    int column = [columnObj intValue];
    int count = [countObj intValue];
    count = count%self.grid.rows;
    if(count < 0){
        count += self.grid.rows;
    }
    printf("moveTilesColumn:%d count:%d\nbefore:", column, count);
    for(int row=0; row<self.grid.rows; ++row){
        printf("\t%d", tilesInfo[row][column]);
        int idx = (row+self.grid.rows-count)%self.grid.rows;
        tilesInfo[row][self.grid.columns] = tilesInfo[idx][column];
    }
    printf("\nafter:");
    for(int row=0; row<self.grid.rows; ++row){
        tilesInfo[row][column] = tilesInfo[row][self.grid.columns];
        printf("\t%d", tilesInfo[row][column]);
        UIImageView * image = [self.images objectAtIndex: tilesInfo[row][column]] ;
        image.frame = [self.grid rectAtRow:row column:column];
    }
    
    CGRect rect = [self.grid rectAtRow:0 column:column];
    rect.origin.y -= self.grid.rowHeight;
    UIImageView * extraImage = [self.extraColumnImages objectAtIndex: column];
    UIImageView * image = [self.images objectAtIndex: tilesInfo[0][column]] ;
    [extraImage setImage:image.image];
    extraImage.frame = rect;
    printf("\n");
}

#pragma mark - Движение строк и столбцов
-(void)drawRow:(NSNumber*)row delta:(NSNumber*)delta{
    for(int column=0; column<self.grid.columns; ++column){
        UIImageView * image = [self.images objectAtIndex: tilesInfo[[row intValue]][column]] ;
        CGRect rect = [self.grid rectAtRow:[row intValue] column:column];
        rect.origin.x += [delta doubleValue];
        if(rect.origin.x+self.grid.columnWidth > self.frame.size.width){
            rect.origin.x -= self.frame.size.width;
        }
        if(rect.origin.x < 0){
            rect.origin.x += self.frame.size.width;
        }
        image.frame = rect;
        
        if(rect.origin.x+self.grid.columnWidth > self.frame.size.width){
            rect.origin.x -= self.frame.size.width;
            UIImageView * extraImage = [self.extraRowImages objectAtIndex: [row intValue]];
            [extraImage setImage:image.image];
            extraImage.frame = rect;
        }
    }
}
-(void)drawColumn:(NSNumber*)column delta:(NSNumber*)delta{
    for(int row=0; row<self.grid.rows; ++row){
        UIImageView * image = [self.images objectAtIndex: tilesInfo[row][[column intValue]]] ;
        CGRect rect = [self.grid rectAtRow:row column:[column intValue]];
        rect.origin.y += [delta doubleValue];
        if(rect.origin.y+self.grid.rowHeight > self.frame.size.height){
            rect.origin.y -= self.frame.size.height;
        }
        if(rect.origin.y < 0){
            rect.origin.y += self.frame.size.height;
        }
        image.frame = rect;
        
        if(rect.origin.y+self.grid.rowHeight > self.frame.size.height){
            rect.origin.y -= self.frame.size.height;
            UIImageView * extraImage = [self.extraColumnImages objectAtIndex: [column intValue]];
            [extraImage setImage:image.image];
            extraImage.frame = rect;
        }
    }
}

#pragma mark - Проверка, завершена ли игра
-(BOOL) isComplete{
    for(int row=0; row<self.grid.rows; ++row){
        for(int column=0; column<self.grid.columns; ++column){
            if(tilesInfo[row][column] != [self.grid indexAtRow:row column:column]){
                return NO;
            }
        }
    }
    return YES;
}


#pragma mark - Метод для получения плитки из фоновой картинки
-(UIImageView *) imageWithRow:(int)row column:(int)column{
    CGRect rect = [self.grid rectAtRow:row column:column];
    UIImageView *image = [[UIImageView alloc] initWithFrame: rect];
    //image.layer.shadowColor = [UIColor blackColor].CGColor;
    //image.layer.shadowOffset = CGSizeMake(1, 1);
    //image.layer.shadowOpacity = 0.5f;
    //image.layer.shadowRadius = 1.0;
    [image setImage: [_background.image cropWithRect:rect fromSize: self.frame.size]];
    return image;
}

#pragma mark - Инициализация
-(id)init{
    self = [super init];
    if (self) {
        srand(time(NULL));
        
        _direction = DIRECTION_UNKNOWN;
        _touchesCounter = 0;
        
        Settings* settings = [Settings settings];
        
        [self setMultipleTouchEnabled:YES];
        
        CGRect bgFrame = self.frame;
        bgFrame.origin.x = 0;
        bgFrame.origin.y = 0;
        
        if(settings.isUniform == YES){
            self.grid = [[Grid alloc]initUniformWithColumns:settings.columns screenSize:bgFrame.size];
        }else{
            self.grid = [[Grid alloc]initWithRows:settings.rows coulumns:settings.columns screenSize:bgFrame.size];
        }
        _background = [[UIImageView alloc] initWithFrame:bgFrame];
        double ratio = self.frame.size.width/self.frame.size.height;
        
        [_background setImage: [settings.image cropForRatio:ratio] ];
        //[self addSubview:_background]; // Фоновая картинка
        
        self.images = [[NSMutableArray alloc] init];
        self.extraRowImages = [[NSMutableArray alloc] init];
        self.extraColumnImages = [[NSMutableArray alloc] init];
        
        for(int row=0; row<self.grid.rows; ++row){
            UIImageView *image = [self imageWithRow:row column:(self.grid.columns-1)];
            CGRect rect = image.frame;
            rect.origin.x += self.grid.columnWidth;
            image.frame = rect;
            [self addSubview: image];
            [self.extraRowImages addObject:image];
        }
        for(int column=0; column<self.grid.columns; ++column){
            UIImageView *image = [self imageWithRow:0 column:column];
            CGRect rect = image.frame;
            rect.origin.y -= self.grid.rowHeight;
            image.frame = rect;
            [self addSubview: image];
            [self.extraColumnImages addObject:image];
        }
        for(int row=0; row<self.grid.rows; ++row){
            for(int column=0; column<self.grid.columns; ++column){
                tilesInfo[row][column] = [self.grid indexAtRow:row column:column];
                UIImageView *image = [self imageWithRow:row column:column];
                [self addSubview: image]; // Основные плитки
                [self.images addObject: image];
            }
        }
        [self mix];
    }
    return self;
}

#pragma mark - Обработка касаний
- (void) touchEvent:(TouchEvent)event touch:(Touch*)touch{
    
    double dx = (touch->current.x - touch->start.x);
    double dy = (touch->current.y - touch->start.y);
    if(event != TOUCH_DOWN && DIRECTION_UNKNOWN == self.direction){
        double r = 10;
        do{
            if(pow(dx, 2)+pow(dy, 2)<pow(r, 2)){
                break;
            }
            if(fabs(dy)>fabs(dx*2)){
                self.direction = DIRECTION_VERTICAL;
                break;
            }
            if(fabs(dx)>fabs(dy*2)){
                self.direction = DIRECTION_HORIZONTAL;
                break;
            }
        }while(NO);
    }
    switch(event){
        case TOUCH_DOWN:
            touch->start = touch->current;
            touch->startCoordinate = touch->currentCoordinate;
            self.touchesCounter++;
            break;
        case TOUCH_UP:
        {
            switch(self.direction){
                case DIRECTION_VERTICAL:
                    [self moveTilesColumn:[NSNumber numberWithInt:touch->startCoordinate.column] count:[NSNumber numberWithInt:round(dy/self.grid.rowHeight)]];
                    break;
                case DIRECTION_HORIZONTAL:
                    [self moveTilesRow:[NSNumber numberWithInt:touch->startCoordinate.row] count:[NSNumber numberWithInt:round(dx/self.grid.columnWidth)]];
                    break;
            }
            self.touchesCounter--;
            if(self.touchesCounter <= 0){
                self.touchesCounter = 0;//  А вдруг?
                self.direction = DIRECTION_UNKNOWN;
                if([self isComplete]){
                    UIAlertView* alert;
                    alert = [[UIAlertView alloc] initWithTitle:@"Отличная работа!" message:@"Все плитки на своих местах!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            }
        }
        break;
        case TOUCH_MOVE:
            {
                switch(self.direction){
                    case DIRECTION_VERTICAL:
                        [self drawColumn:[NSNumber numberWithInt:touch->startCoordinate.column] delta: [NSNumber numberWithDouble:dy]];
                        break;
                    case DIRECTION_HORIZONTAL:
                        [self drawRow:[NSNumber numberWithInt:touch->startCoordinate.row] delta: [NSNumber numberWithDouble:dx]];
                        break;
                    default:
                        return;
                }
            }
            break;
        }
}

- (void) touchHandler: (NSSet *)touches touchEvent:(TouchEvent)event{
    if(isMixed == YES){
        return;
    }
    NSArray *touchesArray = [touches allObjects];
    for(int i=0; i<touchesArray.count; ++i){
        UITouch *touch = [touchesArray objectAtIndex:i];
        CGPoint currentPoint = [touch locationInView:self];
        touchArray[i].current = currentPoint;
        touchArray[i].currentCoordinate = [self.grid coordinateAtPoint: currentPoint];
        [self touchEvent:event touch:&touchArray[i]];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self touchHandler:touches touchEvent:TOUCH_DOWN];
}



- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [self touchHandler:touches touchEvent:TOUCH_MOVE];
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self touchHandler:touches touchEvent:TOUCH_UP];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//}


@end

