//
//  main.m
//  Slide
//
//  Created by Николай on 08.04.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
