//
//  MenuViewController.h
//  Slide
//
//  Created by Николай on 20.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

- (IBAction)onPlayButtonClicked:(id)sender;
- (IBAction)onSettingsButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
