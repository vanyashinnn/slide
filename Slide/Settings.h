//
//  Settings.h
//  Slide
//
//  Created by Николай on 20.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Settings : NSObject
@property (nonatomic, readonly) int rows;
@property (nonatomic, readonly) int columns;
@property (nonatomic, readonly) BOOL isUniform;
@property (nonatomic, readonly) UIImage* image;

-(void) setRows:(int)rows;
-(void) setColumns:(int)columns;
-(void) setIsUniform:(BOOL)isUniform;
-(void) setImage:(UIImage *)image;

+(id) settings;

@end
