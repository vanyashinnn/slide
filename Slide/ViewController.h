//
//  ViewController.h
//  Slide
//
//  Created by Николай on 08.04.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintView.h"

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet PaintView *paintView;

@end

