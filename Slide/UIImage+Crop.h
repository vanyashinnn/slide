//
//  UIImage+Crop.h
//  Slide
//
//  Created by Николай on 11.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)

- (UIImage *)cropWithRect:(CGRect)rect fromSize:(CGSize)size;

// ratio: width/height
- (UIImage *)cropForRatio:(double) ratio;
@end
