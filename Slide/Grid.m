//
//  Grid.m
//  Example1
//
//  Created by Николай on 08.05.17.
//  Copyright © 2017 Experiments. All rights reserved.
//

#import "Grid.h"

@interface Grid(){
    NSMutableArray * _rects;
}
@end

@implementation Grid


#pragma mark initialization interface
-(Grid *)initWithRows:(int)rows
             coulumns:(int)columns
           screenSize:(CGSize)size
      paddingVertical:(double)paddingVertical
    paddingHorizontal:(double)paddingHorizontal{
    if(self = [super init]){
        _columns = columns;
        _rows = rows;
        if(_columns < 1){
            _columns = 1;
        }
        if(_rows < 1){
            _rows = 1;
        }
        _rects = [[NSMutableArray alloc]init];
        _columnWidth = size.width/self.columns;
        _rowHeight = size.height/self.rows;
        
        
        for(int row=0; row<self.rows; ++row){
            for(int column=0; column<self.columns; ++column){
                CGRect rect = CGRectMake(
                    (column)*self.columnWidth+paddingHorizontal/2.0f,
                    (row)*self.rowHeight+paddingVertical/2.0f,
                    self.columnWidth - paddingHorizontal,
                    self.rowHeight - paddingVertical
                );
                [_rects addObject: [NSValue valueWithCGRect:rect]];
            }
        }
        _count = [_rects count];
    }
    return self;
}
-(Grid *)initWithRows:(int)rows coulumns:(int)columns screenSize:(CGSize)size padding:(double)padding{
    return [self initWithRows:rows coulumns:columns screenSize:size paddingVertical:padding paddingHorizontal:padding];
}
-(Grid *)initUniformWithColumns:(int)columns screenSize:(CGSize)size padding:(double)padding{
    double ratio = size.height/size.width;
    int rows = round(columns*ratio);
    return [self initWithRows:rows coulumns:columns screenSize:size paddingVertical:padding paddingHorizontal:padding];
}
-(Grid *)initWithRows:(int)rows coulumns:(int)columns screenSize:(CGSize)size{
    return [self initWithRows:rows coulumns:columns screenSize:size paddingVertical:0.0f paddingHorizontal:0.0f];
}
-(Grid *)initUniformWithColumns:(int)columns screenSize:(CGSize)size{
    return [self initUniformWithColumns:columns screenSize:size padding:0.0f];
}
#pragma mark -
#pragma mark Private interface

#pragma mark -
#pragma mark Public interface

-(CGRect)rectAtIndex:(int)index{
    NSValue *val = [_rects objectAtIndex:index];
    return [val CGRectValue];
}
- (CGRect)rectAtRow:(int)row column:(int)column{
    NSValue *val = [_rects objectAtIndex:(row*self.columns+column)];
    return [val CGRectValue];
}
-(GridCoordinate)coordinateAtPoint:(CGPoint)point{
    GridCoordinate coordinate;
    coordinate.row = floor(point.y/self.rowHeight);
    coordinate.column = floor(point.x/self.columnWidth);
    return coordinate;
    
}
-(int)indexAtRow:(int)row column:(int)column{
    return row*self.columns+column;
}
-(int)indexAtPoint:(CGPoint)point{
    GridCoordinate coordinate = [self coordinateAtPoint:point];
    return [self indexAtRow:coordinate.row column:coordinate.column];
}
@end
