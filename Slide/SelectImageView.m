//
//  SelectImageView.m
//  Slide
//
//  Created by Николай on 11.06.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import "SelectImageView.h"
#import "UIImage+Crop.h"
#import "Grid.h"
#import "Settings.h"

static const int MAX_IMAGE_COUNT = 8;

@interface SelectImageView(){
    UIImageView* _background;
}
@property (strong, nonatomic) Grid * grid;
@property (strong, nonatomic) NSMutableArray * images;

@end

@implementation SelectImageView

#pragma mark - Инициализация
-(id)init{
    self = [super init];
    if (self) {
        CGSize size = self.frame.size;
        
        CGRect bgFrame = self.frame;
        bgFrame.origin.x = 0;
        bgFrame.origin.y = 0;
        self.grid = [[Grid alloc]initWithRows:2 coulumns:4 screenSize:bgFrame.size padding:20.0f];
        _background = [[UIImageView alloc] initWithFrame:bgFrame];
        [self addSubview: _background];

        self.images = [[NSMutableArray alloc] init];
        
        
        for(int row=0; row<self.grid.rows; ++row){
            for(int column=0; column<self.grid.columns; ++column){
                int imageIndex = [self.grid indexAtRow:row column:column];
                if(MAX_IMAGE_COUNT < imageIndex){
                    break;
                }
                CGRect rect = [self.grid rectAtRow:row column:column];
                double ratio = rect.size.width/rect.size.height;
                NSString* imageName = [NSString stringWithFormat:@"%d.png", imageIndex];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame: rect];
                imageView.clipsToBounds = YES;
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [imageView setImage: [UIImage imageNamed:imageName]];
                [self addSubview: imageView];
                [self.images addObject: imageView];
            }
        }
        [self updateBackgroundView];
    }
    return self;
}

-(void) updateBackgroundView{
    Settings *settings = [Settings settings];
    double ratio = self.frame.size.width/self.frame.size.height;
    [_background setImage: [settings.image cropForRatio:ratio] ];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    int idx = [self.grid indexAtPoint:currentPoint];
    Settings *settings = [Settings settings];
    UIImageView *imageView = [self.images objectAtIndex:idx];
    [settings setImage:imageView.image];
    [self updateBackgroundView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
