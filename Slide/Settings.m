//
//  Settings.m
//  Slide
//
//  Created by Николай on 20.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import "Settings.h"

@interface Settings(){
    int _rows;
    int _columns;
    BOOL _isUniform;
    int _imageIdx;
    UIImage *_image;
}
@end

@implementation Settings

+ (id)settings {
    static Settings *settings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        settings = [[self alloc] init];
    });
    return settings;
}


- (id)init {
    if (self = [super init]) {
        _rows = 10;
        _isUniform = YES;
        _columns = 5;
        _imageIdx = 2;
        NSString *imgNmae = [NSString stringWithFormat:@"%d.png", _imageIdx];
        _image = [UIImage imageNamed:imgNmae];
    }
    return self;
}

-(void)setImage:(UIImage *)image{
    _image = image;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


@end
