//
//  ViewController.m
//  Slide
//
//  Created by Николай on 08.04.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

-(instancetype)init{
    //NSLog(@"ViewController init");
    self = [super init];
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    //NSLog(@"ViewController initWithCoder");
    self = [super initWithCoder:aDecoder];
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationItem setTitle:@"Двигайте птитки"];
}
-(void)viewDidAppear:(BOOL)animated{
//    NSLog(@"viewDidAppear");
}
-(void)viewDidLayoutSubviews{
    NSLog(@"viewDidLayoutSubviews");
    [self.paintView init];
}
-(void)viewWillDisappear:(BOOL)animated{
//    NSLog(@"viewWillDisappear");
}
-(void)viewDidDisappear:(BOOL)animated{
//    NSLog(@"viewDidDisappear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
