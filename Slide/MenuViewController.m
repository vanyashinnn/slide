//
//  MenuViewController.m
//  Slide
//
//  Created by Николай on 20.05.17.
//  Copyright © 2017 Николай. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"
#import "UIImage+Crop.h"
#import "Settings.h"

@interface MenuViewController (){
//    BOOL isNeedBuildNewGame;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"MenuViewController viewDidLoad");
    [self.navigationItem setTitle:@"Меню"];
    [self updateBackgroundImage];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self updateBackgroundImage];
}
-(void) updateBackgroundImage{
    Settings *settings = [Settings settings];
    double ratio = self.backgroundImageView.frame.size.width/self.backgroundImageView.frame.size.height;
    [self.backgroundImageView setImage:[settings.image cropForRatio:ratio]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)didMoveToParentViewController:(UIViewController *)parent{
//    if(NULL == parent){
        NSLog(@"didMoveToParentViewController");
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onPlayButtonClicked:(id)sender {
//    isNeedBuildNewGame = YES;
    [self performSegueWithIdentifier:@"ShowGameSegue" sender:self];
}

- (IBAction)onSettingsButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"ShowSettingsSegue" sender:self];
}


//// This will get called too before the view appears
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"ShowGameSegue"] && isNeedBuildNewGame == YES) {
//        NSLog(@"prepareForSegue");
//        ViewController *gameViewController = segue.destinationViewController;
//        [gameViewController.paintView initWithImage:[UIImage imageNamed:@"image.png"]];
//    }
//}
@end
